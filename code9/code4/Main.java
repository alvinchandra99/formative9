public class Main {
    public static void main(String[] args) {
        Server server = new Server();
        Client c1 = new Client();
        Client c2 = new Client();
        Client c3 = new Client();

        server.start();
        c1.start();
        c2.start();
        c3.start();
    }
    
}
